#-------------------------------------------------
#
# Project created by QtCreator 2014-09-26T17:27:59
#
#-------------------------------------------------
cache();
QT       += core gui
QT       += printsupport
QT       += multimedia multimediawidgets
#TEMPLATE = app
#TARGET = camera

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ImageEditor
TEMPLATE = app


SOURCES += main.cpp\
        imageeditor.cpp \
    camera.cpp \
    videosettings.cpp \
    imagesettings.cpp

HEADERS  += imageeditor.h \
    camera.h \
    videosettings.h \
    imagesettings.h

FORMS    += imageeditor.ui \
    camera.ui \
    videosettings.ui \
    imagesettings.ui
