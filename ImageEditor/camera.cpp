#include "camera.h"
#include "ui_camera.h"
#include "videosettings.h"
#include "imagesettings.h"

#include <QMediaService>
#include <QMediaRecorder>
#include <QMediaMetaData>

#include <QMessageBox>
#include <QPalette>
#include <QtWidgets>

Camera::Camera(QMainWindow *mainWindow, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Camera),
    mainWindow(mainWindow),
    camera(0),
    imageCapture(0),
    mediaRecorder(0),
    isCapturingImage(false),
    applicationExiting(false)
{
    ui->setupUi(this);
    viewFinder = new QCameraViewfinder;
    setDeviceAction();
}

Camera::~Camera()
{
    delete ui;
    delete mediaRecorder;
    delete imageCapture;
    delete camera;
}

QCameraViewfinder *Camera::getCameraViewFinder()
{
    return viewFinder;
}

void Camera::setDeviceAction()
{
    //camera action
    startAct = new QAction(tr("Start Camera"),this);
    startAct->setEnabled(false);
    startAct->setShortcut(tr("Ctrl+R"));

    stopAct = new QAction(tr("Stop Camera"),this);
    stopAct->setEnabled(false);
    stopAct->setShortcut(tr("Ctrl+P"));

    settingAct = new QAction(tr("Setting"),this);
    settingAct->setEnabled(true);

    devicesMenu = new QMenu(tr("Devices"));
    devicesMenu->addAction(startAct);
    devicesMenu->addAction(stopAct);
    devicesMenu->addAction(settingAct);
    devicesMenu->addSeparator();

    //camera device
    QByteArray cameraDevice;

    QActionGroup *videoDevicesGroup = new QActionGroup(this);
    videoDevicesGroup->setExclusive(true);
    foreach (const QByteArray &deviceName, QCamera::availableDevices())
    {
        QString description = camera->deviceDescription(deviceName);
        QAction *videoDeviceAction = new QAction(description, videoDevicesGroup);
        videoDeviceAction->setCheckable(true);
        videoDeviceAction->setData(QVariant(deviceName));
        if(cameraDevice.isEmpty()){
            cameraDevice = deviceName;
            videoDeviceAction->setChecked(true);
        }
        devicesMenu->addAction(videoDeviceAction);
    }

    mainWindow->menuBar()->addMenu(devicesMenu);

    connect(videoDevicesGroup, SIGNAL(triggered(QAction*)), SLOT(updateCameraDevice(QAction*)));
    connect(ui->captureModeButton, SIGNAL(toggled(bool)), SLOT(updateCaptureMode(bool)));

#ifdef HAVE_CAMERA_BUTTONS
    //ui->lockButton->hide();
#endif
    setCamera(cameraDevice);
}

void Camera::setCamera(const QByteArray &cameraDevice)
{
    delete mediaRecorder;
    delete imageCapture;
    delete camera;

    //camera
    if(cameraDevice.isEmpty())
        camera = new QCamera();
    else
        camera = new QCamera(cameraDevice);

    connect(camera, SIGNAL(stateChanged(QCamera::State)), this, SLOT(updateCameraState(QCamera::State)));
    connect(camera, SIGNAL(error(QCamera::Error)), this, SLOT(displayCameraError()));

    //mediarecorder
    mediaRecorder = new QMediaRecorder(camera);
    connect(mediaRecorder, SIGNAL(stateChanged(QMediaRecorder::State)),this,
            SLOT(updateRecordState(QMediaRecorder::State)));
    connect(mediaRecorder, SIGNAL(durationChanged(qint64)), this, SLOT(updateRecordTime()));
    connect(mediaRecorder, SIGNAL(error(QMediaRecorder::Error)),this, SLOT(displayRecorderError()));

    mediaRecorder->setMetaData(QMediaMetaData::Title, QVariant(QLatin1String("Test Title")));

    //QLAbel
    connect(ui->exposureCompensationSlider, SIGNAL(valueChanged(int)), SLOT(setExposureCompensation(int)));

    camera->setViewfinder(viewFinder);
    updateCameraState(camera->state());
    updateLockStatus(camera->lockStatus(), QCamera::UserRequest);
    updateRecordState(mediaRecorder->state());

    //image Capture
    imageCapture = new QCameraImageCapture(camera);
    connect(imageCapture, SIGNAL(readyForCaptureChanged(bool)), this, SLOT(readyForCapture(bool)));
    connect(imageCapture, SIGNAL(imageCaptured(int,QImage)), this, SLOT(processCapturedImage(int,QImage&)));
    connect(imageCapture, SIGNAL(imageSaved(int,QString)), this, SLOT(imageSaved(int,QString)));
    connect(imageCapture, SIGNAL(error(int,QCameraImageCapture::Error,QString)), this,
            SLOT(displayCaptureError(int,QCameraImageCapture::Error,QString)));

    connect(camera, SIGNAL(lockStatusChanged(QCamera::LockStatus,QCamera::LockChangeReason)), this,
            SLOT(updateLockStatus(QCamera::LockStatus,QCamera::LockChangeReason)));

    //Userinterface for selecting capturing mode from example they use TabWidget SwitchTab

    //ui->captureModeButton->setText(0, (camera->isCaptureModeSupported(QCamera::CaptureStillImage)));
    //ui->captureModeButton->setTabEnabled(1, (camera->isCaptureModeSupported(QCamera::CaptureVideo)));

    updateCaptureMode(isCapturingImage);
    camera->start();
}

void Camera::keyPressEvent(QKeyEvent *event)
{
    if(event->isAutoRepeat())
        return;
    switch (event->key()) {
    case Qt::Key_CameraFocus:
        displayViewFinder();
        camera->searchAndLock();
        event->accept();
        break;
    case Qt::Key_Camera:
        if(camera->captureMode() == QCamera::CaptureStillImage)
            takeImage();
        else
            if(mediaRecorder->state() == QMediaRecorder::RecordingState)
                stop();
            else
                record();
        event->accept();
        break;
    default:
        QWidget::keyPressEvent(event);
    }
}
void Camera::keyReleaseEvent(QKeyEvent *event)
{
    if(event->isAutoRepeat())
        return;

    switch (event->key()) {
    case Qt::Key_CameraFocus:
        camera->unlock();
        break;
    default:
        QWidget::keyReleaseEvent(event);
    }
}

void Camera::startCamera()
{
    camera->start();
}

void Camera::stopCamera()
{
    camera->stop();
}

void Camera::record()
{
    mediaRecorder->record();
    updateRecordTime();
}

void Camera::pause()
{
    mediaRecorder->pause();
}

void Camera::stop()
{
    mediaRecorder->stop();
}

void Camera::setMute(bool muted)
{
    mediaRecorder->setMuted(muted);
}

void Camera::toggleLock()
{
    switch (camera->lockStatus()) {
    case QCamera::Searching:
    case QCamera::Locked:
        camera->unlock();
        break;
    case QCamera::Unlocked:
        camera->searchAndLock();
        break;
    default:
        break;
    }
}

void Camera::takeImage()
{
    isCapturingImage = true;
    imageCapture->capture();
}

void Camera::displayCaptureError(int id, QCameraImageCapture::Error error, const QString &errorString)
{
    Q_UNUSED(id);
    Q_UNUSED(error);
    QMessageBox::warning(this, tr("Image Capture Error"), errorString);
    isCapturingImage = false;
}

void Camera::configureCaptureSettings()
{
    switch (camera->captureMode()) {
    case QCamera::CaptureStillImage:
        configureImageSettings();
        break;
    case QCamera::CaptureVideo:
        configureVideoSettings();
        break;
    default:
        break;
    }
}

void Camera::configureVideoSettings()
{
    VideoSettings settingsDialog(mediaRecorder);
    settingsDialog.setAudioSettings(audioSettings);
    settingsDialog.setVideoSettings(videoSettings);
    settingsDialog.setFormat(videoContainterFormat);

    if(settingsDialog.exec())
    {
        audioSettings = settingsDialog.audioSettings();
        videoSettings = settingsDialog.videoSettings();
        videoContainterFormat = settingsDialog.format();

        mediaRecorder->setEncodingSettings(audioSettings,videoSettings,videoContainterFormat);
    }
}
void Camera::configureImageSettings()
{
    ImageSettings settingDialog(imageCapture);
    settingDialog.setImageSettings(imageSettings);
    if(settingDialog.exec()){
        imageSettings = settingDialog.imageSettings();
        imageCapture->setEncodingSettings(imageSettings);
    }

}

void Camera::displayRecorderError()
{
    QMessageBox::warning(this, tr("Capture error"), mediaRecorder->errorString());
}
void Camera::displayCameraError()
{
    QMessageBox::warning(this, tr("Camera error"), imageCapture->errorString());
}

void Camera::updateCameraDevice(QAction *action)
{
    setCamera(action->data().toByteArray());
}

void Camera::updateCaptureMode(bool mode)
{
    //int tabindex = ui->captureModeButton->CurrentIndex();
    QCamera::CaptureModes captureMode = mode? QCamera::CaptureStillImage : QCamera::CaptureVideo;
    if(camera->isCaptureModeSupported(captureMode))
        camera->setCaptureMode(captureMode);
    isCapturingImage = mode;
    ui->captureModeButton->setText(mode ? "Image": "Video");
}

void Camera::updateCameraState(QCamera::State state)
{
    switch (state) {
    case QCamera::ActiveState:
        startAct->setEnabled(false);
        stopAct->setEnabled(true);
        ui->captureModeButton->setEnabled(true);
        settingAct->setEnabled(true);
        break;
    case QCamera::UnloadedState:
    case QCamera::LoadedState:
        startAct->setEnabled(true);
        stopAct->setEnabled(false);
        ui->captureModeButton->setEnabled(false);
        settingAct->setEnabled(false);
        break;
    default:
        break;
    }
}

void Camera::updateRecordState(QMediaRecorder::State state)
{
    switch (state) {
    case QMediaRecorder::StoppedState:
        ui->recordButton->setEnabled(true);
        ui->pauseButton->setEnabled(true);
        ui->stopButton->setEnabled(false);
        break;
    case QMediaRecorder::PausedState:
        ui->recordButton->setEnabled(true);
        ui->pauseButton->setEnabled(false);
        ui->stopButton->setEnabled(true);
        break;
    case QMediaRecorder::RecordingState:
        ui->recordButton->setEnabled(false);
        ui->pauseButton->setEnabled(true);
        ui->stopButton->setEnabled(true);
        break;
    default:
        break;
    }
}

void Camera::setExposureCompensation(int index)
{
    camera->exposure()->setExposureCompensation(index*0.5);
}

void Camera::updateRecordTime()
{
    QString str = QString("Record %1 sec").arg(mediaRecorder->duration()/1000);
    setStatusBar(str);
}

void Camera::processCapturedImage(int requestId, const QImage &image)
{
    Q_UNUSED(requestId);
    QImage scaledImage = image.scaled(viewFinder->size(),Qt::KeepAspectRatio, Qt::SmoothTransformation);

    //Show Last Capture Image
    ui->lastImagePreviewLabel->setPixmap(QPixmap::fromImage(scaledImage));
    displayCapturedImage();
    QTimer::singleShot(4000,this,SLOT(displayViewFinder()));

    qDebug() << "Image is captured";
}

void Camera::updateLockStatus(QCamera::LockStatus status, QCamera::LockChangeReason reason)
{
    QColor indicationColor = Qt::black;

    switch (status) {
    case QCamera::Searching:
        indicationColor = Qt::yellow;
        setStatusBar(tr("Focussing... "));
        ui->lockButton->setText(tr("Focussing..."));
        break;
    case QCamera::Locked:
        indicationColor = Qt::darkGreen;
        setStatusBar(tr("Focussed "),2000);
        ui->lockButton->setText(tr("Unlock"));
        break;
    case QCamera::Unlocked:
        indicationColor = reason == QCamera::LockFailed ? Qt::red : Qt::black;
        if(reason == QCamera::LockFailed){
            setStatusBar(tr("Lock Failed "),2000);
        }
        ui->lockButton->setText(tr("Focus"));
        break;
    default:
        break;
    }

    QPalette pallete = ui->lockButton->palette();
    pallete.setColor(QPalette::ButtonText, indicationColor);
    ui->lockButton->setPalette(pallete);
}

void Camera::displayViewFinder()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void Camera::displayCapturedImage()
{
    ui->stackedWidget->setCurrentIndex(1);
}

void Camera::readyForCapture(bool ready)
{
    ui->takeImageButton->setEnabled(ready);
}

void Camera::imageSaved(int id, const QString &filename)
{
    Q_UNUSED(id);
    Q_UNUSED(filename);

    isCapturingImage = false;
    if(applicationExiting)
        close();
}

void Camera::setStatusBar(const QString &str, int timeOut)
{
    if(mainWindow){
        mainWindow->statusBar()->showMessage(str, timeOut);
    }
}

void Camera::closeEvent(QCloseEvent * event)
{
    if(isCapturingImage)
    {
        setEnabled(false);
        applicationExiting = true;
        event->ignore();
    }else{
        event->accept();
    }

}

