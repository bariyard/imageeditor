#ifndef CAMERA_H
#define CAMERA_H

#include <QCamera>
#include <QCameraImageCapture>
#include <QMediaRecorder>
#include <QMenuBar>
#include <QCameraViewfinder>

#include <QWidget>
#include <QMainWindow>

namespace Ui {
class Camera;
}

class Camera : public QWidget
{
    Q_OBJECT

public:
    Camera(QMainWindow *mainWindow, QWidget *parent = 0);
    ~Camera();

    QCameraViewfinder *getCameraViewFinder();
private slots:

    void setCamera(const QByteArray &cameraDevice);

    void startCamera();
    void stopCamera();

    void record();
    void pause();
    void stop();
    void setMute(bool muted);

    void toggleLock();
    void takeImage();
    void displayCaptureError(int, QCameraImageCapture::Error, const QString &errorString);

    void configureCaptureSettings();
    void configureVideoSettings();
    void configureImageSettings();

    void displayRecorderError();
    void displayCameraError();

    void updateCameraDevice(QAction *action);

    void updateCameraState(QCamera::State state);
    void updateCaptureMode(bool mode);
    void updateRecordState(QMediaRecorder::State state);
    void setExposureCompensation(int index);

    void updateRecordTime();

    void processCapturedImage(int requestId, const QImage &image);
    void updateLockStatus(QCamera::LockStatus status, QCamera::LockChangeReason reason);

    void displayViewFinder();
    void displayCapturedImage();

    void readyForCapture(bool ready);
    void imageSaved(int id, const QString &filename);

protected:
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void closeEvent(QCloseEvent *event);

private:
    void setDeviceAction();
    void setStatusBar(const QString &str, int timeOut = 0);

    Ui::Camera *ui;
    QMainWindow *mainWindow;

    QCamera *camera;
    QCameraImageCapture *imageCapture;
    QMediaRecorder *mediaRecorder;
    QCameraViewfinder *viewFinder;

    QImageEncoderSettings imageSettings;
    QAudioEncoderSettings audioSettings;
    QVideoEncoderSettings videoSettings;
    QString videoContainterFormat;
    bool isCapturingImage;
    bool applicationExiting;

    QMenu *devicesMenu;

    //Action

    QAction *startAct;
    QAction *stopAct;
    QAction *settingAct;


};

#endif // CAMERA_H
