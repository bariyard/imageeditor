#include "imageeditor.h"
#include "ui_imageeditor.h"
#include <QtGui>
#include <QFileDialog>
#include <QMessageBox>
#include <QPrintDialog>
#include <QScrollBar>
#include <QDockWidget>

ImageEditor::ImageEditor(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ImageEditor)
{
    ui->setupUi(this);

    createActions();
    createMenus();
    createSideTab();

    setWindowTitle(mainWindowTitle);
    resize(500,400);

    mainWindowTitle = "Image Editor by Bariyard";
    camera = new Camera(this,this);
    //camera->setDeviceAction(menuBar());

    setCentralWidget(camera->getCameraViewFinder());

    /*imageLabel = new QLabel;
    imageLabel->setBackgroundRole(QPalette::Base);
    imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    imageLabel->setScaledContents(true);

    scrollArea = new QScrollArea;
    scrollArea->setBackgroundRole(QPalette::Base);
    scrollArea->setWidget(imageLabel);      //add scroll area to image label
    setCentralWidget(scrollArea);   //set central widget for MainWindow

    */


}

ImageEditor::~ImageEditor()
{
    delete ui;
}


void ImageEditor::open(){
    filePath = QFileDialog::getOpenFileName(this,tr("Open Image File"),
                                                    QDir::currentPath(),tr("Images (*.png *.xpm *.jpg)"));
    QFileInfo fileInfo(filePath);
    mainWindowTitle = fileInfo.fileName();
    setWindowTitle(mainWindowTitle);

    if(!filePath.isEmpty())
    {
        QImage image(filePath);
        currentImage = image;
        if(currentImage.isNull())
        {
            QMessageBox::information(this,tr("Image Editor"),tr("Cannot load %l").arg(filePath));
            return;
        }
        //toMonochrome();
        //toNegative();
        //toShiftColor();
        //toTriColor();
        toExposedEdge();

        //set image
        imageLabel->setPixmap(QPixmap::fromImage(currentImage));
        scaleFactory = 1.0;

        printAct->setEnabled(true);
        fitToWindowAct->setEnabled(true);
        updateAction();

        if(!fitToWindowAct->isChecked())
            imageLabel->adjustSize();
    }

}

void ImageEditor::save()
{
    Q_ASSERT(imageLabel->pixmap());
    QImage image;
    image = imageLabel->pixmap()->toImage();
    if(image.isNull() || filePath.isEmpty())
    {
        QMessageBox::information(this,tr("Image Editor"),tr("Cannot Save %l").arg(filePath));
        return;
    }

    QFileInfo fileInfo(filePath);
    qDebug() << fileInfo.filePath();



    QString filename = QFileDialog::getSaveFileName(this,tr("Save File"),
                                                    QDir::homePath(),tr("Images (*.png *.xpm *.jpg)"));

    if(!filename.isEmpty())
    {
        image.save(filename);

        filePath = filename;
        fileInfo.setFile(filePath);
        mainWindowTitle = fileInfo.fileName();
        setWindowTitle(mainWindowTitle);
    }
}

void ImageEditor::print()
{
    Q_ASSERT(imageLabel->pixmap());
#ifndef QT_NO_PRINTER
    QPrintDialog dialog(&printer,this);
    if(dialog.exec())
    {
        QPainter painter(&printer);
        QRect rect = painter.viewport();
        QSize size = imageLabel->pixmap()->size();
        size.scale(rect.size(),Qt::KeepAspectRatio);
        painter.setViewport(rect.x(),rect.y(),size.width(),size.height());
        painter.setWindow(imageLabel->pixmap()->rect());
        painter.drawPixmap(0,0,*imageLabel->pixmap());
    }
#endif
}

void ImageEditor::exit(){
    qDebug() << "Exit";
}

void ImageEditor::zoomIn()
{
    scaleImage(1.25);
}

void ImageEditor::zoomOut()
{
    scaleImage(0.8);
}

void ImageEditor::rotate(){}
void ImageEditor::normalSize()
{
    imageLabel->adjustSize();
    scaleFactory = 1.0;
}

void ImageEditor::fitToWindow()
{
    bool fitToWindow = fitToWindowAct->isChecked();
    scrollArea->setWidgetResizable(fitToWindow);
    if(!fitToWindow)
    {
        normalSize();
    }
    updateAction();
}

void ImageEditor::about()
{
    QMessageBox::about(this,tr("About Image Editor"), tr("<p>The <b>Image Viewer</b> example shows how to combine QLabel "
                                                         "and QScrollArea to display an image. QLabel is typically used "
                                                         "for displaying a text, but it can also display an image. "
                                                         "QScrollArea provides a scrolling view around another widget. "
                                                         "If the child widget exceeds the size of the frame, QScrollArea "
                                                         "automatically provides scroll bars. </p><p>The example "
                                                         "demonstrates how QLabel's ability to scale its contents "
                                                         "(QLabel::scaledContents), and QScrollArea's ability to "
                                                         "automatically resize its contents "
                                                         "(QScrollArea::widgetResizable), can be used to implement "
                                                         "zooming and scaling features. </p><p>In addition the example "
                                                         "shows how to use QPainter to print an image.</p>"));
}

void ImageEditor::aboutQt()
{

}

void ImageEditor::createActions()
{
    openAct = new QAction(tr("&Open..."), this);
    openAct->setShortcut(tr("Ctrl+O"));
    connect(openAct, SIGNAL(triggered()),this,SLOT(open()));

    saveAct = new QAction(tr("&Save"), this);
    saveAct->setShortcut(tr("Ctrl+S"));
    connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

    printAct = new QAction(tr("&Print..."), this);
    printAct->setShortcut(tr("Ctrl+P"));
    printAct->setEnabled(false);
    connect(printAct,SIGNAL(triggered()),this,SLOT(print()));

#ifndef Q_OS_WIN32
    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcut(tr("Ctrl+Q"));
    connect(exitAct, SIGNAL(triggered()), this, SLOT(exit()));
#endif

    zoomInAct = new QAction(tr("Zoom &In (25%)"), this);
    zoomInAct->setShortcut(tr("Ctrl++"));
    zoomInAct->setEnabled(false);
    connect(zoomInAct, SIGNAL(triggered()), this, SLOT(zoomIn()));

    zoomOutAct = new QAction(tr("Zoom &Out (25%)"), this);
    zoomOutAct->setShortcut(tr("Ctrl+-"));
    zoomOutAct->setEnabled(false);
    connect(zoomOutAct, SIGNAL(triggered()), this, SLOT(zoomOut()));

    //rotateAct = new QAction(tr(""), this);

    normalSizeAct = new QAction(tr("&Normal Size"), this);
    normalSizeAct->setShortcut(tr("Ctrl+N"));
    normalSizeAct->setEnabled(false);
    connect(normalSizeAct, SIGNAL(triggered()), this, SLOT(normalSize()));

    fitToWindowAct = new QAction(tr("&Fit to Window"), this);
    fitToWindowAct->setShortcut(tr("Ctrl+F"));
    fitToWindowAct->setCheckable(true);
    fitToWindowAct->setEnabled(false);
    connect(fitToWindowAct, SIGNAL(triggered()), this, SLOT(fitToWindow()));

    aboutAct = new QAction(tr("&About"), this);
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

    aboutQtAct = new QAction(tr("About &Qt"), this);
    connect(aboutQtAct, SIGNAL(triggered()), this, SLOT(aboutQt()));


}

void ImageEditor::createMenus()
{
    fileMenu = new QMenu(tr("&File"), this);
    fileMenu->addAction(openAct);
    fileMenu->addAction(saveAct);
    fileMenu->addSeparator();
    fileMenu->addAction(printAct);
#ifndef Q_OS_WIN32
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);
#endif

    viewMenu = new QMenu(tr("&View"), this);
    viewMenu->addAction(zoomInAct);
    viewMenu->addAction(zoomOutAct);
    viewMenu->addAction(normalSizeAct);
    viewMenu->addSeparator();
    viewMenu->addAction(fitToWindowAct);

    helpMenu = new QMenu(tr("&Help"), this);
    helpMenu->addAction(aboutAct);
    helpMenu->addAction(aboutQtAct);

    menuBar()->addMenu(fileMenu);
    menuBar()->addMenu(viewMenu);
    menuBar()->addMenu(helpMenu);

}

void ImageEditor::updateAction()
{
    //check current zooming
    zoomInAct->setEnabled(!fitToWindowAct->isChecked());
    zoomOutAct->setEnabled(!fitToWindowAct->isChecked());
    normalSizeAct->setEnabled(!fitToWindowAct->isChecked());
}

void ImageEditor::scaleImage(double factor)
{
    Q_ASSERT(imageLabel->pixmap());
    scaleFactory *= factor;
    imageLabel->resize(scaleFactory *imageLabel->pixmap()->size());
    adjustScrollBar(scrollArea->horizontalScrollBar(), factor);
    adjustScrollBar(scrollArea->verticalScrollBar(),factor);

    zoomInAct->setEnabled(scaleFactory < 3.0);
    zoomOutAct->setEnabled(scaleFactory > 0.333);

}

void ImageEditor::adjustScrollBar(QScrollBar *scrollBar, double factor)
{
    scrollBar->setValue(int(factor * scrollBar->value()+ ((factor - 1) * scrollBar->pageStep()/2)));
}


void ImageEditor::createSideTab()
{
    /*QDockWidget *dock = new QDockWidget(tr("Customize"), this);
    dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    imageCustomizeList = new QListWidget(dock);
    imageCustomizeList->addItems(QStringList()
                                << "John Doe, Harmony Enterprises, 12 Lakeside, Ambleton"
                                << "Jane Doe, Memorabilia, 23 Watersedge, Beaton"
                                << "Tammy Shea, Tiblanka, 38 Sea Views, Carlton"
                                << "Tim Sheen, Caraba Gifts, 48 Ocean Way, Deal"
                                << "Sol Harvey, Chicos Coffee, 53 New Springs, Eccleston"
                                << "Sally Hobart, Tiroli Tea, 67 Long River, Fedula");

    dock->setWidget(imageCustomizeList);
    addDockWidget(Qt::RightDockWidgetArea, dock);
*/

    //QDockWidget *dock = new QDockWidget(tr("Prefrence"),this);

}

void ImageEditor::toMonochrome()
{
    QImage image = currentImage;
    for(int i = 0; i < image.width(); i++)
    {
        for(int j = 0; j < image.height(); j++)
        {
            QRgb currentPixel = image.pixel(i,j);
            QColor pixColor = QColor::fromRgb(currentPixel);
            int colorIntensity = (pixColor.red()+ pixColor.green() + pixColor.blue()) /3.0;
            image.setPixel(i,j,QColor(colorIntensity,colorIntensity,colorIntensity).rgb());
        }
    }
    currentImage = image;

}

void ImageEditor::toNegative()
{
    QImage image = currentImage;
    for(int i = 0; i < image.width(); i++)
    {
        for(int j = 0; j < image.height(); j++)
        {
            QRgb currentPixel = image.pixel(i,j);
            QColor pixColor = QColor::fromRgb(currentPixel);
            image.setPixel(i,j,QColor(255-pixColor.red(),255-pixColor.green(),255-pixColor.blue()).rgb());
        }
    }
    currentImage = image;
}

void ImageEditor::toShiftColor()
{
    QImage image = currentImage;
    for(int i = 0; i < image.width(); i++)
    {
        for(int j = 0; j < image.height(); j++)
        {
            QRgb currentPixel = image.pixel(i,j);
            QColor pixColor = QColor::fromRgb(currentPixel);
            image.setPixel(i,j,QColor(pixColor.blue(),pixColor.red(),pixColor.green()).rgb());
        }
    }
    currentImage = image;
}

void ImageEditor::toTriColor()
{
    QImage image = currentImage;
    for(int i = 0; i < image.width(); i++)
    {
        for(int j = 0; j < image.height(); j++)
        {
            QRgb currentPixel = image.pixel(i,j);
            QColor pixColor = QColor::fromRgb(currentPixel);
            double  r,g,b;
            double colorIntensity;

            r = pixColor.red();
            g = pixColor.green();
            b = pixColor.blue();
            colorIntensity = (r + g + b)/3;
            if(colorIntensity < 85)
            {
                r = 0; b = 0;
                //r *= 0.5; b *= 0.5;
            }
            else if(colorIntensity < 170)
            {
                b = 0; g = 0;
                //b *= 0.5; g *= 0.5;
            }
            else
            {
                r = 0; g = 0;
                //r *= 0.5; g*= 0.5;
            }
            image.setPixel(i,j,QColor(r,g,b).rgb());
        }
    }
    currentImage = image;
}


void ImageEditor::toExposedEdge()
{
    double tresshold = 0.05;        //this is hardcode
    QImage image = currentImage;
    for(int i = 0; i < image.width(); i++)
    {
        for(int j = 0; j < image.height(); j++)
        {
            QRgb currentPixel = image.pixel(i,j);
            QColor pixColor = QColor::fromRgb(currentPixel);
            int currentColorIntensity = (pixColor.red()+ pixColor.green() + pixColor.blue()) /3.0;

            //next below pixcel
            int nextPixcelJ = j;
            if(nextPixcelJ +1 < image.height())
                nextPixcelJ +=1;
            else
                nextPixcelJ = 0;

            QRgb nextPixel = image.pixel(i,nextPixcelJ);
            QColor nextPixColor = QColor::fromRgb(nextPixel);
            int nextColorIntensity = (nextPixColor.red()+ nextPixColor.green() + nextPixColor.blue()) /3.0;

            int r = 0, g = 0, b = 0;
            if(!(abs(currentColorIntensity-nextColorIntensity) > (tresshold *255)))
            {
                r = 255; g = 255; b = 255;
            }
            image.setPixel(i,j,QColor(r,g,b).rgb());
        }
    }
    currentImage = image;

}
