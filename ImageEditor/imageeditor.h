#ifndef IMAGEEDITOR_H
#define IMAGEEDITOR_H

#include <QMainWindow>
#include <QLabel>
#include <QAction>
#include <QScrollArea>
#include <QImage>
#include <QPrinter>
#include <QTabWidget>
#include <QListWidget>

#include "camera.h"


namespace Ui {
class ImageEditor;
}

class ImageEditor : public QMainWindow
{
    Q_OBJECT

public:
    explicit ImageEditor(QWidget *parent = 0);
    ~ImageEditor();

private slots:
    void open();
    void save();
    void print();
    void exit();

    void zoomIn();
    void zoomOut();
    void rotate();
    void normalSize();
    void fitToWindow();
    void about();
    void aboutQt();

private:
    Ui::ImageEditor *ui;

    void createMenus();
    void createActions();
    void updateAction();
    void scaleImage(double factor);
    void adjustScrollBar(QScrollBar *scrollBar, double factor);

    //side tab
    void createSideTab();

    /*------Mock Up Image Processing-------*/
    void toMonochrome();
    void toNegative();
    void toShiftColor();
    void toTriColor();
    void toExposedEdge();
    /*------Mock Up Image Processing-------*/

    QString mainWindowTitle;

    //image
    QString filePath;
    QLabel *imageLabel;
    QImage currentImage;
    QScrollArea *scrollArea;
    double scaleFactory;
#ifndef QT_NO_PRINTER
    QPrinter printer;
#endif

    QAction *openAct;
    QAction *saveAct;
    QAction *printAct;
    QAction *exitAct;

    QAction *zoomInAct;
    QAction *zoomOutAct;
    QAction *rotateAct;
    QAction *normalSizeAct;
    QAction *fitToWindowAct;
    QAction *aboutAct;
    QAction *aboutQtAct;

    QMenu *fileMenu;
    QMenu *viewMenu;
    QMenu *helpMenu;

    QListWidget *imageCustomizeList;
    //Tabwidget
    //QTabWidget *sideTab;


    //camera
    Camera *camera;


};

#endif // IMAGEEDITOR_H
