#ifndef IMAGESETTINGS_H
#define IMAGESETTINGS_H

#include <QDialog>
#include <QCameraImageCapture>
#include <QComboBox>

class QComboBox;
namespace Ui {
class ImageSettings;
}

class ImageSettings : public QDialog
{
    Q_OBJECT

public:
    ImageSettings(QCameraImageCapture *imageCapture, QWidget *parent = 0);
    ~ImageSettings();

    QImageEncoderSettings imageSettings() const;
    void setImageSettings(const QImageEncoderSettings &imageSettings);

protected:
    void changeEvent(QEvent *event);
private:
    QVariant boxValue(const QComboBox *box) const;
    void selectComboBoxItem(QComboBox *box, const QVariant &value);

    Ui::ImageSettings *ui;
    QCameraImageCapture *imageCapture;
};

#endif // IMAGESETTINGS_H
