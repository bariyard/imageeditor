#ifndef VIDEOSETTINGS_H
#define VIDEOSETTINGS_H

#include <QDialog>
#include <QMediaRecorder>
#include <QComboBox>

class QComboBox;
class QMediaRecorder;
namespace Ui { class VideoSettings; }

class VideoSettings : public QDialog
{
    Q_OBJECT

public:
    VideoSettings(QMediaRecorder *mediaRecorder, QWidget *parent = 0);
    ~VideoSettings();
    QAudioEncoderSettings audioSettings() const;
    void setAudioSettings(const QAudioEncoderSettings &audioSettings);
    QVideoEncoderSettings videoSettings() const;
    void setVideoSettings(const QVideoEncoderSettings &videoSettings);

    QString format() const;
    void setFormat(const QString &format);

protected:
    void changeEvent(QEvent *event);
private:
    QVariant boxValue(const QComboBox *box) const;
    void selectComboBoxItem(QComboBox *box, const QVariant &value);

    Ui::VideoSettings *ui;
    QMediaRecorder *mediaRecorder;
};

#endif // VIDEOSETTINGS_H
